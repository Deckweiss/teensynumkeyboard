/*
                    The HumbleHacker Keyboard Project
                 Copyright � 2008-2010, David Whetstone
               david DOT whetstone AT humblehacker DOT com

  This file is a part of The HumbleHacker Keyboard Project.

  The HumbleHacker Keyboard Project is free software: you can redistribute
  it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  The HumbleHacker Keyboard Project is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
  Public License for more details.

  You should have received a copy of the GNU General Public License along
  with The HumbleHacker Keyboard Project.  If not, see
  <http://www.gnu.org/licenses/>.

*/

#include <string.h>
#include "binding.h"
#include "keymaps.h"

/*
 *   PreMods
 */

                          /* 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F */
static uint8_t bitcount[] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};

static inline
uint8_t hi_nibble(uint8_t val)
{
  return (val & 0xF0) >> 4;
}

static inline
uint8_t lo_nibble(uint8_t val)
{
  return val & 0x0F;
}

uint8_t
PreMods__compare(const PreMods *this, uint8_t mods)
{
  uint8_t count = 0;
  uint8_t lo_mods = lo_nibble(mods);
  uint8_t hi_mods = hi_nibble(mods);
  uint8_t lo_std  = lo_nibble(this->std);
  uint8_t hi_std  = hi_nibble(this->std);
  count += bitcount[lo_mods&lo_std];
  count += bitcount[hi_mods&hi_std];
  count += bitcount[((lo_mods&~lo_std)|(hi_mods&~hi_std))&lo_nibble(this->any)];
  return count;
}

bool
PreMods__is_empty(const PreMods *this)
{
  return this->std == NONE && this->any == NONE;
}

/*
 *    KeyBinding
 */

void
KeyBinding__copy(const KeyBinding *this, KeyBinding *dst)
{
  memcpy(dst, this, sizeof(KeyBinding));
}

const ModeTarget*
KeyBinding__get_mode_target(const KeyBinding *this)
{
  static ModeTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)this->target, sizeof(ModeTarget));
  return &target;
}

const MacroTarget*
KeyBinding__get_macro_target(const KeyBinding *this)
{
  static MacroTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)this->target, sizeof(MacroTarget));
  return &target;
}

const MapTarget*
KeyBinding__get_map_target(const KeyBinding *this)
{
  static MapTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)this->target, sizeof(MapTarget));
  return &target;
}

const ModifierTarget*
KeyBinding__get_modifier_target(const KeyBinding *this)
{
  static ModifierTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)this->target, sizeof(ModifierTarget));
  return &target;
}

/*
 *    KeyBindingArray
 */

const KeyBinding*
KeyBindingArray__get_binding(const KeyBindingArray *this, uint8_t index)
{
  static KeyBinding binding;
  static const KeyBinding *last_binding = NULL;
  if (&this->data[index] != last_binding)
  {
    memcpy_P((void*)&binding, (PGM_VOID_P)&this->data[index], sizeof(KeyBinding));
    last_binding = &this->data[index];
  }
  return &binding;
}

/*
 *    MacroTarget
 */

const MapTarget*
MacroTarget__get_map_target(const MacroTarget *this, uint8_t index)
{
  static MapTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)&this->targets[index], sizeof(MapTarget));
  return &target;
}

/*
 *    All Bindings
 */


const MapTarget      US_SLA_0000 PROGMEM = { 0x000, HID_USAGE__SLASH_AND__QUESTION };
const MapTarget      US_1_0000 PROGMEM = { 0x000, HID_USAGE_1_AND__EXCLAMATION };
const MapTarget      US_0_0000 PROGMEM = { 0x000, HID_USAGE_0_AND__RPAREN };
const MapTarget      US_EQL_0000 PROGMEM = { 0x000, HID_USAGE__EQUALS_AND__PLUS };
const MapTarget      US_2_0000 PROGMEM = { 0x000, HID_USAGE_2_AND__ATSIGN };
const MapTarget      US_5_0000 PROGMEM = { 0x000, HID_USAGE_5_AND__PERCENT };
const MapTarget      US_4_0000 PROGMEM = { 0x000, HID_USAGE_4_AND__DOLLAR };
const MapTarget      US_7_0000 PROGMEM = { 0x000, HID_USAGE_7_AND__AMPERSAND };
const MapTarget      US_6_0000 PROGMEM = { 0x000, HID_USAGE_6_AND__CARET };
const MapTarget      US_9_0000 PROGMEM = { 0x000, HID_USAGE_9_AND__LPAREN };
const MapTarget      US_8_0000 PROGMEM = { 0x000, HID_USAGE_8_AND__ASTERISK };
const MapTarget      US_C_0000 PROGMEM = { 0x000, HID_USAGE_C_AND_C };
const MapTarget      US_Y_0000 PROGMEM = { 0x000, HID_USAGE_Y_AND_Y };
const MapTarget      US_A_0000 PROGMEM = { 0x000, HID_USAGE_A_AND_A };
const MapTarget      US_APO_0000 PROGMEM = { 0x000, HID_USAGE_SQUOTE_AND_DQUOTE };
const MapTarget      US_MIN_0000 PROGMEM = { 0x000, HID_USAGE__MINUS_AND_UNDERSCORE };
const MapTarget      US_B_0000 PROGMEM = { 0x000, HID_USAGE_B_AND_B };
const MapTarget      US_E_0000 PROGMEM = { 0x000, HID_USAGE_E_AND_E };
const MapTarget      US_D_0000 PROGMEM = { 0x000, HID_USAGE_D_AND_D };
const MapTarget      US_G_0000 PROGMEM = { 0x000, HID_USAGE_G_AND_G };
const MapTarget      US_F_0000 PROGMEM = { 0x000, HID_USAGE_F_AND_F };
const MapTarget      US_I_0000 PROGMEM = { 0x000, HID_USAGE_I_AND_I };
const MapTarget      US_H_0000 PROGMEM = { 0x000, HID_USAGE_H_AND_H };
const MapTarget      US_K_0000 PROGMEM = { 0x000, HID_USAGE_K_AND_K };
const MapTarget      US_J_0000 PROGMEM = { 0x000, HID_USAGE_J_AND_J };
const MapTarget      US_M_0000 PROGMEM = { 0x000, HID_USAGE_M_AND_M };
const MapTarget      US_L_0000 PROGMEM = { 0x000, HID_USAGE_L_AND_L };
const MapTarget      US_O_0000 PROGMEM = { 0x000, HID_USAGE_O_AND_O };
const MapTarget      US_N_0000 PROGMEM = { 0x000, HID_USAGE_N_AND_N };
const MapTarget      US_Q_0000 PROGMEM = { 0x000, HID_USAGE_Q_AND_Q };
const MapTarget      US_P_0000 PROGMEM = { 0x000, HID_USAGE_P_AND_P };
const MapTarget      US_S_0000 PROGMEM = { 0x000, HID_USAGE_S_AND_S };
const MapTarget      US_R_0000 PROGMEM = { 0x000, HID_USAGE_R_AND_R };
const MapTarget      US_U_0000 PROGMEM = { 0x000, HID_USAGE_U_AND_U };
const MapTarget      US_T_0000 PROGMEM = { 0x000, HID_USAGE_T_AND_T };
const MapTarget      US_W_0000 PROGMEM = { 0x000, HID_USAGE_W_AND_W };
const MapTarget      US_V_0000 PROGMEM = { 0x000, HID_USAGE_V_AND_V };
const MapTarget      US_PER_0000 PROGMEM = { 0x000, HID_USAGE__PERIOD_AND__GREATERTHAN };
const MapTarget      US_X_0000 PROGMEM = { 0x000, HID_USAGE_X_AND_X };
const MapTarget      US_SEM_0000 PROGMEM = { 0x000, HID_USAGE__SEMICOLON_AND__COLON };
const MapTarget      US_Z_0000 PROGMEM = { 0x000, HID_USAGE_Z_AND_Z };
const MapTarget      US_COM_0000 PROGMEM = { 0x000, HID_USAGE__COMMA_AND__LESSTHAN };
const MapTarget      US_BSL_0000 PROGMEM = { 0x000, HID_USAGE__BACKSLASH_AND__PIPE };
const MapTarget      US_3_0000 PROGMEM = { 0x000, HID_USAGE_3_AND__POUND };
const MapTarget      US_BQ_0000 PROGMEM = { 0x000, HID_USAGE__BACKTICK_AND__TILDE };
const MapTarget      Common_ESC_0000 PROGMEM = { 0x000, HID_USAGE_ESCAPE };
const ModifierTarget Common_LCT_0000 PROGMEM = { L_CTL };
const MapTarget      Common_LBR_0000 PROGMEM = { 0x000, HID_USAGE__LSQUAREBRACKET_AND__LCURLYBRACE };
const ModifierTarget Common_LAL_0000 PROGMEM = { L_ALT };
const MapTarget      Common_DWN_0000 PROGMEM = { 0x000, HID_USAGE_DOWNARROW };
const ModifierTarget Common_RAL_0000 PROGMEM = { R_ALT };
const ModifierTarget Common_RCT_0000 PROGMEM = { R_CTL };
const MapTarget      Common_RET_0000 PROGMEM = { 0x000, HID_USAGE_RETURN_OR_ENTER };
const MapTarget      Common_BAK_0000 PROGMEM = { 0x000, HID_USAGE_BACKSPACE };
const MapTarget      Common_F2_0000 PROGMEM = { 0x000, HID_USAGE_F2 };
const MapTarget      Common_SLK_0000 PROGMEM = { 0x000, HID_USAGE_SCROLL_LOCK };
const MapTarget      Common_F4_0000 PROGMEM = { 0x000, HID_USAGE_F4 };
const MapTarget      Common_F6_0000 PROGMEM = { 0x000, HID_USAGE_F6 };
const ModeTarget     Common_PRG_0000 PROGMEM = { MOMENTARY, keymap_Fn }; 
const ModifierTarget Common_RSH_0000 PROGMEM = { R_SHF };
const MapTarget      Common_PGD_0000 PROGMEM = { 0x000, HID_USAGE_PAGEDOWN };
const MapTarget      Common_F12_0000 PROGMEM = { 0x000, HID_USAGE_F12 };
const MapTarget      Common_DEL_0000 PROGMEM = { 0x000, HID_USAGE_DELETE_FORWARD };
const MapTarget      Common_F8_0000 PROGMEM = { 0x000, HID_USAGE_F8 };
const ModifierTarget Common_LSH_0000 PROGMEM = { L_SHF };
const MapTarget      Common_F11_0000 PROGMEM = { 0x000, HID_USAGE_F11 };
const MapTarget      Common_LFT_0000 PROGMEM = { 0x000, HID_USAGE_LEFTARROW };
const MapTarget      Common_PRT_0000 PROGMEM = { 0x000, HID_USAGE_PRINTSCREEN };
const ModeTarget     Common_KEY_0000 PROGMEM = { TOGGLE, keymap_KeyPad }; 
const MapTarget      Common_F10_0000 PROGMEM = { 0x000, HID_USAGE_F10 };
const MapTarget      Common_UP_0000 PROGMEM = { 0x000, HID_USAGE_UPARROW };
const MapTarget      Common_PAU_0000 PROGMEM = { 0x000, HID_USAGE_KEYBOARD_PAUSE };
const MapTarget      Common_END_0000 PROGMEM = { 0x000, HID_USAGE_END };
const MapTarget      Common_PGU_0000 PROGMEM = { 0x000, HID_USAGE_PAGEUP };
const MapTarget      Common_F3_0000 PROGMEM = { 0x000, HID_USAGE_F3 };
const MapTarget      Common_F5_0000 PROGMEM = { 0x000, HID_USAGE_F5 };
const ModifierTarget Common_WIN_0000 PROGMEM = { R_GUI };
const MapTarget      Common_F7_0000 PROGMEM = { 0x000, HID_USAGE_F7 };
const MapTarget      Common_TAB_0000 PROGMEM = { 0x000, HID_USAGE_TAB };
const MapTarget      Common_SPC_0000 PROGMEM = { 0x000, HID_USAGE_SPACEBAR };
const MapTarget      Common_HOM_0000 PROGMEM = { 0x000, HID_USAGE_HOME };
const MapTarget      Common_RT_0000 PROGMEM = { 0x000, HID_USAGE_RIGHTARROW };
const MapTarget      Common_F1_0000 PROGMEM = { 0x000, HID_USAGE_F1 };
const MapTarget      Common_INS_0000 PROGMEM = { 0x000, HID_USAGE_INSERT };
const MapTarget      Common_CAP_0000 PROGMEM = { 0x000, HID_USAGE_CAPS_LOCK };
const MapTarget      Common_RBR_0000 PROGMEM = { 0x000, HID_USAGE__RSQUAREBRACKET_AND__RCURLYBRACE };
const MapTarget      Common_F9_0000 PROGMEM = { 0x000, HID_USAGE_F9 };
const MapTarget      Fn_KEY_0000Targets[] PROGMEM =
{
  { 0x20, HID_USAGE_H_AND_H },
  { 0x00, HID_USAGE_U_AND_U },
  { 0x00, HID_USAGE_M_AND_M },
  { 0x00, HID_USAGE_B_AND_B },
  { 0x00, HID_USAGE_L_AND_L },
  { 0x00, HID_USAGE_E_AND_E },
  { 0x20, HID_USAGE_H_AND_H },
  { 0x00, HID_USAGE_A_AND_A },
  { 0x00, HID_USAGE_C_AND_C },
  { 0x00, HID_USAGE_K_AND_K },
  { 0x00, HID_USAGE_E_AND_E },
  { 0x00, HID_USAGE_D_AND_D },
  { 0x00, HID_USAGE_SPACEBAR },
  { 0x20, HID_USAGE_K_AND_K },
  { 0x00, HID_USAGE_I_AND_I },
  { 0x00, HID_USAGE_N_AND_N },
  { 0x00, HID_USAGE_E_AND_E },
  { 0x00, HID_USAGE_S_AND_S },
  { 0x00, HID_USAGE_I_AND_I },
  { 0x00, HID_USAGE_S_AND_S },
  { 0x00, HID_USAGE_SPACEBAR },
  { 0x20, HID_USAGE_C_AND_C },
  { 0x00, HID_USAGE_O_AND_O },
  { 0x00, HID_USAGE_N_AND_N },
  { 0x00, HID_USAGE_T_AND_T },
  { 0x00, HID_USAGE_O_AND_O },
  { 0x00, HID_USAGE_U_AND_U },
  { 0x00, HID_USAGE_R_AND_R },
  { 0x00, HID_USAGE_E_AND_E },
  { 0x00, HID_USAGE_D_AND_D },
  { 0x00, HID_USAGE_SPACEBAR },
  { 0x00, HID_USAGE_K_AND_K },
  { 0x00, HID_USAGE_E_AND_E },
  { 0x00, HID_USAGE_Y_AND_Y },
  { 0x00, HID_USAGE_B_AND_B },
  { 0x00, HID_USAGE_O_AND_O },
  { 0x00, HID_USAGE_A_AND_A },
  { 0x00, HID_USAGE_R_AND_R },
  { 0x00, HID_USAGE_D_AND_D },
  { 0x00, HID_USAGE_RETURN_OR_ENTER },
};

const MacroTarget    Fn_KEY_0000 PROGMEM = { 40, &Fn_KEY_0000Targets[0] }; 
const ModeTarget     Fn_F1_0000 PROGMEM = { TOGGLE, keymap_US }; 
const ModeTarget     Fn_F2_0000 PROGMEM = { TOGGLE, keymap_DV }; 
const MapTarget      KeyPad_LBR_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD__PERIOD_AND_DELETE };
const MapTarget      KeyPad_I_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_8_AND_UP_ARROW };
const MapTarget      KeyPad_K_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_5 };
const MapTarget      KeyPad_J_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_4_AND_LEFT_ARROW };
const MapTarget      KeyPad_M_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_1_AND_END };
const MapTarget      KeyPad_L_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_6_AND_RIGHT_ARROW };
const MapTarget      KeyPad_O_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_9_AND_PAGEUP };
const MapTarget      KeyPad_P_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD__MINUS };
const MapTarget      KeyPad_U_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_7_AND_HOME };
const MapTarget      KeyPad_7_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_NUM_LOCK_AND_CLEAR };
const MapTarget      KeyPad_SPC_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_0_AND_INSERT };
const MapTarget      KeyPad_9_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD__SLASH };
const MapTarget      KeyPad_8_0000 PROGMEM = { 0x000, HID_USAGE__EQUALS_AND__PLUS };
const MapTarget      KeyPad_SLA_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_ENTER };
const MapTarget      KeyPad_SEM_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD__PLUS };
const MapTarget      KeyPad_COM_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_2_AND_DOWN_ARROW };
const MapTarget      KeyPad_0_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD__ASTERISK };
const MapTarget      KeyPad_RBR_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_ENTER };
const MapTarget      KeyPad_PER_0000 PROGMEM = { 0x000, HID_USAGE_KEYPAD_3_AND_PAGEDN };
const MapTarget      DV_C_0000 PROGMEM = { 0x000, HID_USAGE_J_AND_J };
const MapTarget      DV_B_0000 PROGMEM = { 0x000, HID_USAGE_X_AND_X };
const MapTarget      DV_E_0000 PROGMEM = { 0x000, HID_USAGE__PERIOD_AND__GREATERTHAN };
const MapTarget      DV_D_0000 PROGMEM = { 0x000, HID_USAGE_E_AND_E };
const MapTarget      DV_G_0000 PROGMEM = { 0x000, HID_USAGE_I_AND_I };
const MapTarget      DV_F_0000 PROGMEM = { 0x000, HID_USAGE_U_AND_U };
const MapTarget      DV_I_0000 PROGMEM = { 0x000, HID_USAGE_C_AND_C };
const MapTarget      DV_H_0000 PROGMEM = { 0x000, HID_USAGE_D_AND_D };
const MapTarget      DV_K_0000 PROGMEM = { 0x000, HID_USAGE_T_AND_T };
const MapTarget      DV_J_0000 PROGMEM = { 0x000, HID_USAGE_H_AND_H };
const MapTarget      DV_L_0000 PROGMEM = { 0x000, HID_USAGE_N_AND_N };
const MapTarget      DV_O_0000 PROGMEM = { 0x000, HID_USAGE_R_AND_R };
const MapTarget      DV_N_0000 PROGMEM = { 0x000, HID_USAGE_B_AND_B };
const MapTarget      DV_Q_0000 PROGMEM = { 0x000, HID_USAGE_SQUOTE_AND_DQUOTE };
const MapTarget      DV_P_0000 PROGMEM = { 0x000, HID_USAGE_L_AND_L };
const MapTarget      DV_S_0000 PROGMEM = { 0x000, HID_USAGE_O_AND_O };
const MapTarget      DV_R_0000 PROGMEM = { 0x000, HID_USAGE_P_AND_P };
const MapTarget      DV_U_0000 PROGMEM = { 0x000, HID_USAGE_G_AND_G };
const MapTarget      DV_T_0000 PROGMEM = { 0x000, HID_USAGE_Y_AND_Y };
const MapTarget      DV_W_0000 PROGMEM = { 0x000, HID_USAGE__COMMA_AND__LESSTHAN };
const MapTarget      DV_V_0000 PROGMEM = { 0x000, HID_USAGE_K_AND_K };
const MapTarget      DV_PER_0000 PROGMEM = { 0x000, HID_USAGE_V_AND_V };
const MapTarget      DV_X_0000 PROGMEM = { 0x000, HID_USAGE_Q_AND_Q };
const MapTarget      DV_Z_0000 PROGMEM = { 0x000, HID_USAGE__SEMICOLON_AND__COLON };
const MapTarget      DV_COM_0000 PROGMEM = { 0x000, HID_USAGE_W_AND_W };
const MapTarget      DV_Y_0000 PROGMEM = { 0x000, HID_USAGE_F_AND_F };
const MapTarget      DV_SLA_0000 PROGMEM = { 0x000, HID_USAGE_Z_AND_Z };
const MapTarget      DV_SEM_0000 PROGMEM = { 0x000, HID_USAGE_S_AND_S };

/*
 *    Aggregated bindings per key
 */


const KeyBinding US_SLA[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_SLA_0000 }, 
};

const KeyBinding US_1[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_1_0000 }, 
};

const KeyBinding US_0[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_0_0000 }, 
};

const KeyBinding US_EQL[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_EQL_0000 }, 
};

const KeyBinding US_2[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_2_0000 }, 
};

const KeyBinding US_5[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_5_0000 }, 
};

const KeyBinding US_4[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_4_0000 }, 
};

const KeyBinding US_7[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_7_0000 }, 
};

const KeyBinding US_6[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_6_0000 }, 
};

const KeyBinding US_9[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_9_0000 }, 
};

const KeyBinding US_8[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_8_0000 }, 
};

const KeyBinding US_C[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_C_0000 }, 
};

const KeyBinding US_Y[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_Y_0000 }, 
};

const KeyBinding US_A[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_A_0000 }, 
};

const KeyBinding US_APO[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_APO_0000 }, 
};

const KeyBinding US_MIN[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_MIN_0000 }, 
};

const KeyBinding US_B[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_B_0000 }, 
};

const KeyBinding US_E[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_E_0000 }, 
};

const KeyBinding US_D[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_D_0000 }, 
};

const KeyBinding US_G[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_G_0000 }, 
};

const KeyBinding US_F[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_F_0000 }, 
};

const KeyBinding US_I[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_I_0000 }, 
};

const KeyBinding US_H[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_H_0000 }, 
};

const KeyBinding US_K[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_K_0000 }, 
};

const KeyBinding US_J[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_J_0000 }, 
};

const KeyBinding US_M[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_M_0000 }, 
};

const KeyBinding US_L[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_L_0000 }, 
};

const KeyBinding US_O[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_O_0000 }, 
};

const KeyBinding US_N[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_N_0000 }, 
};

const KeyBinding US_Q[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_Q_0000 }, 
};

const KeyBinding US_P[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_P_0000 }, 
};

const KeyBinding US_S[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_S_0000 }, 
};

const KeyBinding US_R[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_R_0000 }, 
};

const KeyBinding US_U[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_U_0000 }, 
};

const KeyBinding US_T[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_T_0000 }, 
};

const KeyBinding US_W[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_W_0000 }, 
};

const KeyBinding US_V[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_V_0000 }, 
};

const KeyBinding US_PER[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_PER_0000 }, 
};

const KeyBinding US_X[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_X_0000 }, 
};

const KeyBinding US_SEM[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_SEM_0000 }, 
};

const KeyBinding US_Z[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_Z_0000 }, 
};

const KeyBinding US_COM[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_COM_0000 }, 
};

const KeyBinding US_BSL[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_BSL_0000 }, 
};

const KeyBinding US_3[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_3_0000 }, 
};

const KeyBinding US_BQ[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&US_BQ_0000 }, 
};

const KeyBinding Common_ESC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_ESC_0000 }, 
};

const KeyBinding Common_LCT[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&Common_LCT_0000 }, 
};

const KeyBinding Common_LBR[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_LBR_0000 }, 
};

const KeyBinding Common_LAL[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&Common_LAL_0000 }, 
};

const KeyBinding Common_DWN[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_DWN_0000 }, 
};

const KeyBinding Common_RAL[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&Common_RAL_0000 }, 
};

const KeyBinding Common_RCT[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&Common_RCT_0000 }, 
};

const KeyBinding Common_RET[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_RET_0000 }, 
};

const KeyBinding Common_BAK[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_BAK_0000 }, 
};

const KeyBinding Common_F2[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F2_0000 }, 
};

const KeyBinding Common_SLK[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_SLK_0000 }, 
};

const KeyBinding Common_F4[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F4_0000 }, 
};

const KeyBinding Common_F6[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F6_0000 }, 
};

const KeyBinding Common_PRG[] PROGMEM =
{
  { MODE, {0x00, 0x00}, (void*)&Common_PRG_0000 }, 
};

const KeyBinding Common_RSH[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&Common_RSH_0000 }, 
};

const KeyBinding Common_PGD[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_PGD_0000 }, 
};

const KeyBinding Common_F12[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F12_0000 }, 
};

const KeyBinding Common_DEL[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_DEL_0000 }, 
};

const KeyBinding Common_F8[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F8_0000 }, 
};

const KeyBinding Common_LSH[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&Common_LSH_0000 }, 
};

const KeyBinding Common_F11[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F11_0000 }, 
};

const KeyBinding Common_LFT[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_LFT_0000 }, 
};

const KeyBinding Common_PRT[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_PRT_0000 }, 
};

const KeyBinding Common_KEY[] PROGMEM =
{
  { MODE, {0x00, 0x00}, (void*)&Common_KEY_0000 }, 
};

const KeyBinding Common_F10[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F10_0000 }, 
};

const KeyBinding Common_UP[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_UP_0000 }, 
};

const KeyBinding Common_PAU[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_PAU_0000 }, 
};

const KeyBinding Common_END[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_END_0000 }, 
};

const KeyBinding Common_PGU[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_PGU_0000 }, 
};

const KeyBinding Common_F3[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F3_0000 }, 
};

const KeyBinding Common_F5[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F5_0000 }, 
};

const KeyBinding Common_WIN[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&Common_WIN_0000 }, 
};

const KeyBinding Common_F7[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F7_0000 }, 
};

const KeyBinding Common_TAB[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_TAB_0000 }, 
};

const KeyBinding Common_SPC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_SPC_0000 }, 
};

const KeyBinding Common_HOM[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_HOM_0000 }, 
};

const KeyBinding Common_RT[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_RT_0000 }, 
};

const KeyBinding Common_F1[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F1_0000 }, 
};

const KeyBinding Common_INS[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_INS_0000 }, 
};

const KeyBinding Common_CAP[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_CAP_0000 }, 
};

const KeyBinding Common_RBR[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_RBR_0000 }, 
};

const KeyBinding Common_F9[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Common_F9_0000 }, 
};

const KeyBinding Fn_KEY[] PROGMEM =
{
  { MACRO, {0x00, 0x00}, (void*)&Fn_KEY_0000 }, 
};

const KeyBinding Fn_F1[] PROGMEM =
{
  { MODE, {0x00, 0x00}, (void*)&Fn_F1_0000 }, 
};

const KeyBinding Fn_F2[] PROGMEM =
{
  { MODE, {0x00, 0x00}, (void*)&Fn_F2_0000 }, 
};

const KeyBinding KeyPad_LBR[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_LBR_0000 }, 
};

const KeyBinding KeyPad_I[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_I_0000 }, 
};

const KeyBinding KeyPad_K[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_K_0000 }, 
};

const KeyBinding KeyPad_J[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_J_0000 }, 
};

const KeyBinding KeyPad_M[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_M_0000 }, 
};

const KeyBinding KeyPad_L[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_L_0000 }, 
};

const KeyBinding KeyPad_O[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_O_0000 }, 
};

const KeyBinding KeyPad_P[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_P_0000 }, 
};

const KeyBinding KeyPad_U[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_U_0000 }, 
};

const KeyBinding KeyPad_7[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_7_0000 }, 
};

const KeyBinding KeyPad_SPC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_SPC_0000 }, 
};

const KeyBinding KeyPad_9[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_9_0000 }, 
};

const KeyBinding KeyPad_8[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_8_0000 }, 
};

const KeyBinding KeyPad_SLA[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_SLA_0000 }, 
};

const KeyBinding KeyPad_SEM[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_SEM_0000 }, 
};

const KeyBinding KeyPad_COM[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_COM_0000 }, 
};

const KeyBinding KeyPad_0[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_0_0000 }, 
};

const KeyBinding KeyPad_RBR[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_RBR_0000 }, 
};

const KeyBinding KeyPad_PER[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&KeyPad_PER_0000 }, 
};

const KeyBinding DV_C[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_C_0000 }, 
};

const KeyBinding DV_B[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_B_0000 }, 
};

const KeyBinding DV_E[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_E_0000 }, 
};

const KeyBinding DV_D[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_D_0000 }, 
};

const KeyBinding DV_G[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_G_0000 }, 
};

const KeyBinding DV_F[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_F_0000 }, 
};

const KeyBinding DV_I[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_I_0000 }, 
};

const KeyBinding DV_H[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_H_0000 }, 
};

const KeyBinding DV_K[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_K_0000 }, 
};

const KeyBinding DV_J[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_J_0000 }, 
};

const KeyBinding DV_L[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_L_0000 }, 
};

const KeyBinding DV_O[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_O_0000 }, 
};

const KeyBinding DV_N[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_N_0000 }, 
};

const KeyBinding DV_Q[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_Q_0000 }, 
};

const KeyBinding DV_P[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_P_0000 }, 
};

const KeyBinding DV_S[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_S_0000 }, 
};

const KeyBinding DV_R[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_R_0000 }, 
};

const KeyBinding DV_U[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_U_0000 }, 
};

const KeyBinding DV_T[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_T_0000 }, 
};

const KeyBinding DV_W[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_W_0000 }, 
};

const KeyBinding DV_V[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_V_0000 }, 
};

const KeyBinding DV_PER[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_PER_0000 }, 
};

const KeyBinding DV_X[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_X_0000 }, 
};

const KeyBinding DV_Z[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_Z_0000 }, 
};

const KeyBinding DV_COM[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_COM_0000 }, 
};

const KeyBinding DV_Y[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_Y_0000 }, 
};

const KeyBinding DV_SLA[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_SLA_0000 }, 
};

const KeyBinding DV_SEM[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&DV_SEM_0000 }, 
};



