/*
                    The HumbleHacker Keyboard Project
                 Copyright � 2008-2010, David Whetstone
               david DOT whetstone AT humblehacker DOT com

  This file is a part of The HumbleHacker Keyboard Project.

  The HumbleHacker Keyboard Project is free software: you can redistribute
  it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  The HumbleHacker Keyboard Project is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
  Public License for more details.

  You should have received a copy of the GNU General Public License along
  with The HumbleHacker Keyboard Project.  If not, see
  <http://www.gnu.org/licenses/>.

*/

#ifndef __BINDING_H__
#define __BINDING_H__

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdbool.h>
#include <stdint.h>
#include "hid_usages.h"
#include "matrix.h"

typedef struct ModeTarget ModeTarget;
typedef struct MacroTarget MacroTarget;
typedef struct MapTarget MapTarget;
typedef struct ModifierTarget ModifierTarget;
typedef struct KeyBinding KeyBinding;
typedef struct KeyBindingArray KeyBindingArray;
typedef struct PreMods PreMods;
typedef const KeyBindingArray* KeyMap;

/*
 *    PreMods
 */

struct PreMods
{
  uint8_t std;
  uint8_t any;
};

uint8_t PreMods__compare(const PreMods *this, uint8_t mods);
bool PreMods__is_empty(const PreMods *this);

/*
 *    KeyBinding
 */

struct KeyBinding
{
  enum {NOMAP, MAP, MODE, MACRO, MODIFIER} kind;
  PreMods premods;
  void *target;
};

void                  KeyBinding__copy(const KeyBinding *this, KeyBinding *dst);
const ModeTarget*     KeyBinding__get_mode_target(const KeyBinding *this);
const MacroTarget*    KeyBinding__get_macro_target(const KeyBinding *this);
const MapTarget*      KeyBinding__get_map_target(const KeyBinding *this);
const ModifierTarget* KeyBinding__get_modifier_target(const KeyBinding *this);

/*
 *    KeyBindingArray
 */

struct KeyBindingArray
{
  uint8_t length;
  const KeyBinding *data;
} ;

const KeyBinding* KeyBindingArray__get_binding(const KeyBindingArray *this, uint8_t index);

static inline
void
KeyBindingArray__get(KeyBindingArray *array, const KeyBindingArray *from)
{
  memcpy_P((void*)array, (PGM_VOID_P)from, sizeof(KeyBindingArray));
}

/*
 *    ModeTarget
 */

struct ModeTarget
{
  enum {MOMENTARY, TOGGLE} type;
  KeyMap mode_map;
};

/*
 *    MapTarget
 */

struct MapTarget
{
  uint8_t modifiers;
  Usage usage;
};

/*
 *    ModifierTarget
 */

struct ModifierTarget
{
  Modifier modifier;
};

/*
 *    MacroTarget
 */

struct MacroTarget
{
  uint8_t length;
  const MapTarget *targets;
};

const MapTarget* MacroTarget__get_map_target(const MacroTarget *this, uint8_t index);

/*
 *    Binding declarations
 */

extern const KeyBinding US_SLA[] PROGMEM;
extern const KeyBinding US_1[] PROGMEM;
extern const KeyBinding US_0[] PROGMEM;
extern const KeyBinding US_EQL[] PROGMEM;
extern const KeyBinding US_2[] PROGMEM;
extern const KeyBinding US_5[] PROGMEM;
extern const KeyBinding US_4[] PROGMEM;
extern const KeyBinding US_7[] PROGMEM;
extern const KeyBinding US_6[] PROGMEM;
extern const KeyBinding US_9[] PROGMEM;
extern const KeyBinding US_8[] PROGMEM;
extern const KeyBinding US_C[] PROGMEM;
extern const KeyBinding US_Y[] PROGMEM;
extern const KeyBinding US_A[] PROGMEM;
extern const KeyBinding US_APO[] PROGMEM;
extern const KeyBinding US_MIN[] PROGMEM;
extern const KeyBinding US_B[] PROGMEM;
extern const KeyBinding US_E[] PROGMEM;
extern const KeyBinding US_D[] PROGMEM;
extern const KeyBinding US_G[] PROGMEM;
extern const KeyBinding US_F[] PROGMEM;
extern const KeyBinding US_I[] PROGMEM;
extern const KeyBinding US_H[] PROGMEM;
extern const KeyBinding US_K[] PROGMEM;
extern const KeyBinding US_J[] PROGMEM;
extern const KeyBinding US_M[] PROGMEM;
extern const KeyBinding US_L[] PROGMEM;
extern const KeyBinding US_O[] PROGMEM;
extern const KeyBinding US_N[] PROGMEM;
extern const KeyBinding US_Q[] PROGMEM;
extern const KeyBinding US_P[] PROGMEM;
extern const KeyBinding US_S[] PROGMEM;
extern const KeyBinding US_R[] PROGMEM;
extern const KeyBinding US_U[] PROGMEM;
extern const KeyBinding US_T[] PROGMEM;
extern const KeyBinding US_W[] PROGMEM;
extern const KeyBinding US_V[] PROGMEM;
extern const KeyBinding US_PER[] PROGMEM;
extern const KeyBinding US_X[] PROGMEM;
extern const KeyBinding US_SEM[] PROGMEM;
extern const KeyBinding US_Z[] PROGMEM;
extern const KeyBinding US_COM[] PROGMEM;
extern const KeyBinding US_BSL[] PROGMEM;
extern const KeyBinding US_3[] PROGMEM;
extern const KeyBinding US_BQ[] PROGMEM;
extern const KeyBinding Common_ESC[] PROGMEM;
extern const KeyBinding Common_LCT[] PROGMEM;
extern const KeyBinding Common_LBR[] PROGMEM;
extern const KeyBinding Common_LAL[] PROGMEM;
extern const KeyBinding Common_DWN[] PROGMEM;
extern const KeyBinding Common_RAL[] PROGMEM;
extern const KeyBinding Common_RCT[] PROGMEM;
extern const KeyBinding Common_RET[] PROGMEM;
extern const KeyBinding Common_BAK[] PROGMEM;
extern const KeyBinding Common_F2[] PROGMEM;
extern const KeyBinding Common_SLK[] PROGMEM;
extern const KeyBinding Common_F4[] PROGMEM;
extern const KeyBinding Common_F6[] PROGMEM;
extern const KeyBinding Common_PRG[] PROGMEM;
extern const KeyBinding Common_RSH[] PROGMEM;
extern const KeyBinding Common_PGD[] PROGMEM;
extern const KeyBinding Common_F12[] PROGMEM;
extern const KeyBinding Common_DEL[] PROGMEM;
extern const KeyBinding Common_F8[] PROGMEM;
extern const KeyBinding Common_LSH[] PROGMEM;
extern const KeyBinding Common_F11[] PROGMEM;
extern const KeyBinding Common_LFT[] PROGMEM;
extern const KeyBinding Common_PRT[] PROGMEM;
extern const KeyBinding Common_KEY[] PROGMEM;
extern const KeyBinding Common_F10[] PROGMEM;
extern const KeyBinding Common_UP[] PROGMEM;
extern const KeyBinding Common_PAU[] PROGMEM;
extern const KeyBinding Common_END[] PROGMEM;
extern const KeyBinding Common_PGU[] PROGMEM;
extern const KeyBinding Common_F3[] PROGMEM;
extern const KeyBinding Common_F5[] PROGMEM;
extern const KeyBinding Common_WIN[] PROGMEM;
extern const KeyBinding Common_F7[] PROGMEM;
extern const KeyBinding Common_TAB[] PROGMEM;
extern const KeyBinding Common_SPC[] PROGMEM;
extern const KeyBinding Common_HOM[] PROGMEM;
extern const KeyBinding Common_RT[] PROGMEM;
extern const KeyBinding Common_F1[] PROGMEM;
extern const KeyBinding Common_INS[] PROGMEM;
extern const KeyBinding Common_CAP[] PROGMEM;
extern const KeyBinding Common_RBR[] PROGMEM;
extern const KeyBinding Common_F9[] PROGMEM;
extern const KeyBinding Fn_KEY[] PROGMEM;
extern const KeyBinding Fn_F1[] PROGMEM;
extern const KeyBinding Fn_F2[] PROGMEM;
extern const KeyBinding KeyPad_LBR[] PROGMEM;
extern const KeyBinding KeyPad_I[] PROGMEM;
extern const KeyBinding KeyPad_K[] PROGMEM;
extern const KeyBinding KeyPad_J[] PROGMEM;
extern const KeyBinding KeyPad_M[] PROGMEM;
extern const KeyBinding KeyPad_L[] PROGMEM;
extern const KeyBinding KeyPad_O[] PROGMEM;
extern const KeyBinding KeyPad_P[] PROGMEM;
extern const KeyBinding KeyPad_U[] PROGMEM;
extern const KeyBinding KeyPad_7[] PROGMEM;
extern const KeyBinding KeyPad_SPC[] PROGMEM;
extern const KeyBinding KeyPad_9[] PROGMEM;
extern const KeyBinding KeyPad_8[] PROGMEM;
extern const KeyBinding KeyPad_SLA[] PROGMEM;
extern const KeyBinding KeyPad_SEM[] PROGMEM;
extern const KeyBinding KeyPad_COM[] PROGMEM;
extern const KeyBinding KeyPad_0[] PROGMEM;
extern const KeyBinding KeyPad_RBR[] PROGMEM;
extern const KeyBinding KeyPad_PER[] PROGMEM;
extern const KeyBinding DV_C[] PROGMEM;
extern const KeyBinding DV_B[] PROGMEM;
extern const KeyBinding DV_E[] PROGMEM;
extern const KeyBinding DV_D[] PROGMEM;
extern const KeyBinding DV_G[] PROGMEM;
extern const KeyBinding DV_F[] PROGMEM;
extern const KeyBinding DV_I[] PROGMEM;
extern const KeyBinding DV_H[] PROGMEM;
extern const KeyBinding DV_K[] PROGMEM;
extern const KeyBinding DV_J[] PROGMEM;
extern const KeyBinding DV_L[] PROGMEM;
extern const KeyBinding DV_O[] PROGMEM;
extern const KeyBinding DV_N[] PROGMEM;
extern const KeyBinding DV_Q[] PROGMEM;
extern const KeyBinding DV_P[] PROGMEM;
extern const KeyBinding DV_S[] PROGMEM;
extern const KeyBinding DV_R[] PROGMEM;
extern const KeyBinding DV_U[] PROGMEM;
extern const KeyBinding DV_T[] PROGMEM;
extern const KeyBinding DV_W[] PROGMEM;
extern const KeyBinding DV_V[] PROGMEM;
extern const KeyBinding DV_PER[] PROGMEM;
extern const KeyBinding DV_X[] PROGMEM;
extern const KeyBinding DV_Z[] PROGMEM;
extern const KeyBinding DV_COM[] PROGMEM;
extern const KeyBinding DV_Y[] PROGMEM;
extern const KeyBinding DV_SLA[] PROGMEM;
extern const KeyBinding DV_SEM[] PROGMEM;

#endif // __BINDING_H__
