/*
                    The HumbleHacker Keyboard Project
                 Copyright � 2008-2010, David Whetstone
               david DOT whetstone AT humblehacker DOT com

  This file is a part of The HumbleHacker Keyboard Project.

  The HumbleHacker Keyboard Project is free software: you can redistribute
  it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  The HumbleHacker Keyboard Project is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
  Public License for more details.

  You should have received a copy of the GNU General Public License along
  with The HumbleHacker Keyboard Project.  If not, see
  <http://www.gnu.org/licenses/>.

*/

#include "keymaps.h"
#include "hid_usages.h"

const KeyBindingArray keymap_US[] =
{

  /* col: 0 */
  /* row:0 loc:ESC */ {1, &Common_ESC[0]},
  /* row:1 loc:Q */ {1, &US_Q[0]},
  /* row:2 loc:W */ {1, &US_W[0]},
  /* row:3 loc:E */ {1, &US_E[0]},
  /* row:4 loc:R */ {1, &US_R[0]},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:A */ {1, &US_A[0]},
  /* row:2 loc:S */ {1, &US_S[0]},
  /* row:3 loc:D */ {1, &US_D[0]},
  /* row:4 loc:F */ {1, &US_F[0]},
  /* row:5 loc:T */ {1, &US_T[0]},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:Z */ {1, &US_Z[0]},
  /* row:2 loc:X */ {1, &US_X[0]},
  /* row:3 loc:C */ {1, &US_C[0]},
  /* row:4 loc:V */ {1, &US_V[0]},
  /* row:5 loc:SPC */ {1, &Common_SPC[0]},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:EQL */ {1, &US_EQL[0]},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:RET */ {1, &Common_RET[0]},
  /* row:4 loc:BAK */ {1, &Common_BAK[0]},
  /* row:5 loc:G */ {1, &US_G[0]},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:B */ {1, &US_B[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:N */ {1, &US_N[0]},
  /* row:7 loc:I */ {1, &US_I[0]},
  /* row:8 loc:U */ {1, &US_U[0]},
  /* row:9 loc:O */ {1, &US_O[0]},
  /* row:10 loc:P */ {1, &US_P[0]},
  /* row:11 loc:RBR */ {1, &Common_RBR[0]},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:Y */ {1, &US_Y[0]},
  /* row:7 loc:J */ {1, &US_J[0]},
  /* row:8 loc:K */ {1, &US_K[0]},
  /* row:9 loc:L */ {1, &US_L[0]},
  /* row:10 loc:LBR */ {1, &Common_LBR[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:LSH */ {1, &Common_LSH[0]},
  /* row:7 loc:M */ {1, &US_M[0]},
  /* row:8 loc:COM */ {1, &US_COM[0]},
  /* row:9 loc:PER */ {1, &US_PER[0]},
  /* row:10 loc:SLA */ {1, &US_SLA[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 8 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:LCT */ {1, &Common_LCT[0]},
  /* row:8 loc:LAL */ {1, &Common_LAL[0]},
  /* row:9 loc:RAL */ {1, &Common_RAL[0]},
  /* row:10 loc:WIN */ {1, &Common_WIN[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 9 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:H */ {1, &US_H[0]},
  /* row:11 loc:-- */ {0, NULL},
};

const KeyMap default_keymap = &keymap_US[0];

const KeyBindingArray keymap_Common[] =
{

  /* col: 0 */
  /* row:0 loc:ESC */ {1, &Common_ESC[0]},
  /* row:1 loc:Q */ {0, NULL},
  /* row:2 loc:W */ {0, NULL},
  /* row:3 loc:E */ {0, NULL},
  /* row:4 loc:R */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:A */ {0, NULL},
  /* row:2 loc:S */ {0, NULL},
  /* row:3 loc:D */ {0, NULL},
  /* row:4 loc:F */ {0, NULL},
  /* row:5 loc:T */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:Z */ {0, NULL},
  /* row:2 loc:X */ {0, NULL},
  /* row:3 loc:C */ {0, NULL},
  /* row:4 loc:V */ {0, NULL},
  /* row:5 loc:SPC */ {1, &Common_SPC[0]},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:EQL */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:RET */ {1, &Common_RET[0]},
  /* row:4 loc:BAK */ {1, &Common_BAK[0]},
  /* row:5 loc:G */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:B */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:N */ {0, NULL},
  /* row:7 loc:I */ {0, NULL},
  /* row:8 loc:U */ {0, NULL},
  /* row:9 loc:O */ {0, NULL},
  /* row:10 loc:P */ {0, NULL},
  /* row:11 loc:RBR */ {1, &Common_RBR[0]},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:Y */ {0, NULL},
  /* row:7 loc:J */ {0, NULL},
  /* row:8 loc:K */ {0, NULL},
  /* row:9 loc:L */ {0, NULL},
  /* row:10 loc:LBR */ {1, &Common_LBR[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:LSH */ {1, &Common_LSH[0]},
  /* row:7 loc:M */ {0, NULL},
  /* row:8 loc:COM */ {0, NULL},
  /* row:9 loc:PER */ {0, NULL},
  /* row:10 loc:SLA */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 8 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:LCT */ {1, &Common_LCT[0]},
  /* row:8 loc:LAL */ {1, &Common_LAL[0]},
  /* row:9 loc:RAL */ {1, &Common_RAL[0]},
  /* row:10 loc:WIN */ {1, &Common_WIN[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 9 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:H */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},
};


const KeyBindingArray keymap_Fn[] =
{

  /* col: 0 */
  /* row:0 loc:ESC */ {0, NULL},
  /* row:1 loc:Q */ {0, NULL},
  /* row:2 loc:W */ {0, NULL},
  /* row:3 loc:E */ {0, NULL},
  /* row:4 loc:R */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:A */ {0, NULL},
  /* row:2 loc:S */ {0, NULL},
  /* row:3 loc:D */ {0, NULL},
  /* row:4 loc:F */ {0, NULL},
  /* row:5 loc:T */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:Z */ {0, NULL},
  /* row:2 loc:X */ {0, NULL},
  /* row:3 loc:C */ {0, NULL},
  /* row:4 loc:V */ {0, NULL},
  /* row:5 loc:SPC */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:EQL */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:RET */ {0, NULL},
  /* row:4 loc:BAK */ {0, NULL},
  /* row:5 loc:G */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:B */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:N */ {0, NULL},
  /* row:7 loc:I */ {0, NULL},
  /* row:8 loc:U */ {0, NULL},
  /* row:9 loc:O */ {0, NULL},
  /* row:10 loc:P */ {0, NULL},
  /* row:11 loc:RBR */ {0, NULL},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:Y */ {0, NULL},
  /* row:7 loc:J */ {0, NULL},
  /* row:8 loc:K */ {0, NULL},
  /* row:9 loc:L */ {0, NULL},
  /* row:10 loc:LBR */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:LSH */ {0, NULL},
  /* row:7 loc:M */ {0, NULL},
  /* row:8 loc:COM */ {0, NULL},
  /* row:9 loc:PER */ {0, NULL},
  /* row:10 loc:SLA */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 8 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:LCT */ {0, NULL},
  /* row:8 loc:LAL */ {0, NULL},
  /* row:9 loc:RAL */ {0, NULL},
  /* row:10 loc:WIN */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 9 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:H */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},
};


const KeyBindingArray keymap_KeyPad[] =
{

  /* col: 0 */
  /* row:0 loc:ESC */ {1, &Common_ESC[0]},
  /* row:1 loc:Q */ {0, NULL},
  /* row:2 loc:W */ {0, NULL},
  /* row:3 loc:E */ {0, NULL},
  /* row:4 loc:R */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:A */ {0, NULL},
  /* row:2 loc:S */ {0, NULL},
  /* row:3 loc:D */ {0, NULL},
  /* row:4 loc:F */ {0, NULL},
  /* row:5 loc:T */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:Z */ {0, NULL},
  /* row:2 loc:X */ {0, NULL},
  /* row:3 loc:C */ {0, NULL},
  /* row:4 loc:V */ {0, NULL},
  /* row:5 loc:SPC */ {1, &KeyPad_SPC[0]},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:EQL */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:RET */ {1, &Common_RET[0]},
  /* row:4 loc:BAK */ {1, &Common_BAK[0]},
  /* row:5 loc:G */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:B */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:N */ {0, NULL},
  /* row:7 loc:I */ {1, &KeyPad_I[0]},
  /* row:8 loc:U */ {1, &KeyPad_U[0]},
  /* row:9 loc:O */ {1, &KeyPad_O[0]},
  /* row:10 loc:P */ {1, &KeyPad_P[0]},
  /* row:11 loc:RBR */ {1, &KeyPad_RBR[0]},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:Y */ {0, NULL},
  /* row:7 loc:J */ {1, &KeyPad_J[0]},
  /* row:8 loc:K */ {1, &KeyPad_K[0]},
  /* row:9 loc:L */ {1, &KeyPad_L[0]},
  /* row:10 loc:LBR */ {1, &KeyPad_LBR[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:LSH */ {1, &Common_LSH[0]},
  /* row:7 loc:M */ {1, &KeyPad_M[0]},
  /* row:8 loc:COM */ {1, &KeyPad_COM[0]},
  /* row:9 loc:PER */ {1, &KeyPad_PER[0]},
  /* row:10 loc:SLA */ {1, &KeyPad_SLA[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 8 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:LCT */ {1, &Common_LCT[0]},
  /* row:8 loc:LAL */ {1, &Common_LAL[0]},
  /* row:9 loc:RAL */ {1, &Common_RAL[0]},
  /* row:10 loc:WIN */ {1, &Common_WIN[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 9 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:H */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},
};


const KeyBindingArray keymap_DV[] =
{

  /* col: 0 */
  /* row:0 loc:ESC */ {1, &Common_ESC[0]},
  /* row:1 loc:Q */ {1, &DV_Q[0]},
  /* row:2 loc:W */ {1, &DV_W[0]},
  /* row:3 loc:E */ {1, &DV_E[0]},
  /* row:4 loc:R */ {1, &DV_R[0]},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:A */ {1, &US_A[0]},
  /* row:2 loc:S */ {1, &DV_S[0]},
  /* row:3 loc:D */ {1, &DV_D[0]},
  /* row:4 loc:F */ {1, &DV_F[0]},
  /* row:5 loc:T */ {1, &DV_T[0]},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:Z */ {1, &DV_Z[0]},
  /* row:2 loc:X */ {1, &DV_X[0]},
  /* row:3 loc:C */ {1, &DV_C[0]},
  /* row:4 loc:V */ {1, &DV_V[0]},
  /* row:5 loc:SPC */ {1, &Common_SPC[0]},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:EQL */ {1, &US_EQL[0]},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:RET */ {1, &Common_RET[0]},
  /* row:4 loc:BAK */ {1, &Common_BAK[0]},
  /* row:5 loc:G */ {1, &DV_G[0]},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:B */ {1, &DV_B[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:N */ {1, &DV_N[0]},
  /* row:7 loc:I */ {1, &DV_I[0]},
  /* row:8 loc:U */ {1, &DV_U[0]},
  /* row:9 loc:O */ {1, &DV_O[0]},
  /* row:10 loc:P */ {1, &DV_P[0]},
  /* row:11 loc:RBR */ {1, &Common_RBR[0]},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:Y */ {1, &DV_Y[0]},
  /* row:7 loc:J */ {1, &DV_J[0]},
  /* row:8 loc:K */ {1, &DV_K[0]},
  /* row:9 loc:L */ {1, &DV_L[0]},
  /* row:10 loc:LBR */ {1, &Common_LBR[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:LSH */ {1, &Common_LSH[0]},
  /* row:7 loc:M */ {1, &US_M[0]},
  /* row:8 loc:COM */ {1, &DV_COM[0]},
  /* row:9 loc:PER */ {1, &DV_PER[0]},
  /* row:10 loc:SLA */ {1, &DV_SLA[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 8 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:LCT */ {1, &Common_LCT[0]},
  /* row:8 loc:LAL */ {1, &Common_LAL[0]},
  /* row:9 loc:RAL */ {1, &Common_RAL[0]},
  /* row:10 loc:WIN */ {1, &Common_WIN[0]},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 9 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:H */ {1, &DV_H[0]},
  /* row:11 loc:-- */ {0, NULL},
};


