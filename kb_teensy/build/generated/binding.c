/*
                    The HumbleHacker Keyboard Project
                 Copyright � 2008-2010, David Whetstone
               david DOT whetstone AT humblehacker DOT com

  This file is a part of The HumbleHacker Keyboard Project.

  The HumbleHacker Keyboard Project is free software: you can redistribute
  it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  The HumbleHacker Keyboard Project is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
  Public License for more details.

  You should have received a copy of the GNU General Public License along
  with The HumbleHacker Keyboard Project.  If not, see
  <http://www.gnu.org/licenses/>.

*/

#include <string.h>
#include "binding.h"
#include "keymaps.h"

/*
 *   PreMods
 */

                          /* 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F */
static uint8_t bitcount[] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};

static inline
uint8_t hi_nibble(uint8_t val)
{
  return (val & 0xF0) >> 4;
}

static inline
uint8_t lo_nibble(uint8_t val)
{
  return val & 0x0F;
}

uint8_t
PreMods__compare(const PreMods *this, uint8_t mods)
{
  uint8_t count = 0;
  uint8_t lo_mods = lo_nibble(mods);
  uint8_t hi_mods = hi_nibble(mods);
  uint8_t lo_std  = lo_nibble(this->std);
  uint8_t hi_std  = hi_nibble(this->std);
  count += bitcount[lo_mods&lo_std];
  count += bitcount[hi_mods&hi_std];
  count += bitcount[((lo_mods&~lo_std)|(hi_mods&~hi_std))&lo_nibble(this->any)];
  return count;
}

bool
PreMods__is_empty(const PreMods *this)
{
  return this->std == NONE && this->any == NONE;
}

/*
 *    KeyBinding
 */

void
KeyBinding__copy(const KeyBinding *this, KeyBinding *dst)
{
  memcpy(dst, this, sizeof(KeyBinding));
}

const ModeTarget*
KeyBinding__get_mode_target(const KeyBinding *this)
{
  static ModeTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)this->target, sizeof(ModeTarget));
  return &target;
}

const MacroTarget*
KeyBinding__get_macro_target(const KeyBinding *this)
{
  static MacroTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)this->target, sizeof(MacroTarget));
  return &target;
}

const MapTarget*
KeyBinding__get_map_target(const KeyBinding *this)
{
  static MapTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)this->target, sizeof(MapTarget));
  return &target;
}

const ModifierTarget*
KeyBinding__get_modifier_target(const KeyBinding *this)
{
  static ModifierTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)this->target, sizeof(ModifierTarget));
  return &target;
}

/*
 *    KeyBindingArray
 */

const KeyBinding*
KeyBindingArray__get_binding(const KeyBindingArray *this, uint8_t index)
{
  static KeyBinding binding;
  static const KeyBinding *last_binding = NULL;
  if (&this->data[index] != last_binding)
  {
    memcpy_P((void*)&binding, (PGM_VOID_P)&this->data[index], sizeof(KeyBinding));
    last_binding = &this->data[index];
  }
  return &binding;
}

/*
 *    MacroTarget
 */

const MapTarget*
MacroTarget__get_map_target(const MacroTarget *this, uint8_t index)
{
  static MapTarget target;
  memcpy_P((void*)&target, (PGM_VOID_P)&this->targets[index], sizeof(MapTarget));
  return &target;
}

/*
 *    All Bindings
 */


const MapTarget      CommonOwn_ESC_0000 PROGMEM = { 0x000, HID_USAGE_ESCAPE };
const MapTarget      CommonOwn_BAK_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const ModifierTarget CommonOwn_MT_0000 PROGMEM = { R_GUI };
const ModeTarget     CommonOwn_FN_0000 PROGMEM = { MOMENTARY, keymap_FnOwn }; 
const ModeTarget     CommonOwn_NUM_0000 PROGMEM = { MOMENTARY, keymap_NumbersOwn }; 
const ModifierTarget CommonOwn_RAL_0000 PROGMEM = { R_ALT };
const MapTarget      CommonOwn_SPC_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const ModifierTarget CommonOwn_RCT_0000 PROGMEM = { R_CTL };
const ModifierTarget CommonOwn_RSH_0000 PROGMEM = { R_SHF };
const MapTarget      CommonOwn_RET_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const ModeTarget     CommonOwn_ALG_0000 PROGMEM = { MOMENTARY, keymap_ALTGR }; 
const MapTarget      FnOwn_Q_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      FnOwn_BRC_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      FnOwn_R_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      FnOwn_U_0000 PROGMEM = { 0x000, HID_USAGE__LSQUAREBRACKET_AND__LCURLYBRACE };
const MapTarget      FnOwn_T_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      FnOwn_BRO_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      FnOwn_Y_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      FnOwn_ESC_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      FnOwn_O_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      FnOwn_EQL_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      Own_A_0000 PROGMEM = { 0x000, HID_USAGE_A_AND_A };
const MapTarget      Own_S_0000 PROGMEM = { 0x000, HID_USAGE_S_AND_S };
const MapTarget      Own_BRC_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      Own_B_0000 PROGMEM = { 0x000, HID_USAGE_B_AND_B };
const MapTarget      Own_E_0000 PROGMEM = { 0x000, HID_USAGE_E_AND_E };
const MapTarget      Own_D_0000 PROGMEM = { 0x000, HID_USAGE_D_AND_D };
const MapTarget      Own_G_0000 PROGMEM = { 0x000, HID_USAGE_G_AND_G };
const MapTarget      Own_BRO_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      Own_I_0000 PROGMEM = { 0x000, HID_USAGE_I_AND_I };
const MapTarget      Own_H_0000 PROGMEM = { 0x000, HID_USAGE_H_AND_H };
const MapTarget      Own_K_0000 PROGMEM = { 0x000, HID_USAGE_K_AND_K };
const MapTarget      Own_J_0000 PROGMEM = { 0x000, HID_USAGE_J_AND_J };
const MapTarget      Own_M_0000 PROGMEM = { 0x000, HID_USAGE_M_AND_M };
const MapTarget      Own_L_0000 PROGMEM = { 0x000, HID_USAGE_L_AND_L };
const MapTarget      Own_O_0000 PROGMEM = { 0x000, HID_USAGE_O_AND_O };
const MapTarget      Own_N_0000 PROGMEM = { 0x000, HID_USAGE_N_AND_N };
const MapTarget      Own_Q_0000 PROGMEM = { 0x000, HID_USAGE_Q_AND_Q };
const MapTarget      Own_P_0000 PROGMEM = { 0x000, HID_USAGE_P_AND_P };
const MapTarget      Own_EQL_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      Own_R_0000 PROGMEM = { 0x000, HID_USAGE_R_AND_R };
const MapTarget      Own_U_0000 PROGMEM = { 0x000, HID_USAGE_U_AND_U };
const MapTarget      Own_T_0000 PROGMEM = { 0x000, HID_USAGE_T_AND_T };
const MapTarget      Own_W_0000 PROGMEM = { 0x000, HID_USAGE_W_AND_W };
const MapTarget      Own_V_0000 PROGMEM = { 0x000, HID_USAGE_V_AND_V };
const MapTarget      Own_PER_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      Own_X_0000 PROGMEM = { 0x000, HID_USAGE_X_AND_X };
const MapTarget      Own_Y_0000 PROGMEM = { 0x000, HID_USAGE_Y_AND_Y };
const MapTarget      Own_Z_0000 PROGMEM = { 0x000, HID_USAGE_Z_AND_Z };
const MapTarget      Own_COM_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      Own_QUE_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      Own_F_0000 PROGMEM = { 0x000, HID_USAGE_F_AND_F };
const MapTarget      Own_C_0000 PROGMEM = { 0x000, HID_USAGE_C_AND_C };
const MapTarget      NumbersOwn_A_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_BRC_0000 PROGMEM = { 0x000, HID_USAGE_F12 };
const MapTarget      NumbersOwn_E_0000 PROGMEM = { 0x000, HID_USAGE_F3 };
const MapTarget      NumbersOwn_D_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_G_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_F_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_I_0000 PROGMEM = { 0x000, HID_USAGE_F8 };
const MapTarget      NumbersOwn_H_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_K_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_J_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_L_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_O_0000 PROGMEM = { 0x000, HID_USAGE_F9 };
const MapTarget      NumbersOwn_Q_0000 PROGMEM = { 0x000, HID_USAGE_F1 };
const MapTarget      NumbersOwn_P_0000 PROGMEM = { 0x000, HID_USAGE_F10 };
const MapTarget      NumbersOwn_S_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_R_0000 PROGMEM = { 0x000, HID_USAGE_F4 };
const MapTarget      NumbersOwn_U_0000 PROGMEM = { 0x000, HID_USAGE_F7 };
const MapTarget      NumbersOwn_T_0000 PROGMEM = { 0x000, HID_USAGE_F5 };
const MapTarget      NumbersOwn_W_0000 PROGMEM = { 0x000, HID_USAGE_F2 };
const MapTarget      NumbersOwn_Z_0000 PROGMEM = { 0x000, HID_USAGE_F6 };
const MapTarget      NumbersOwn_ESC_0000 PROGMEM = { 0x000, HID_USAGE_RESERVED__LPARENNO_EVENT_INDICATED_RPAREN };
const MapTarget      NumbersOwn_BRO_0000 PROGMEM = { 0x000, HID_USAGE_F11 };

/*
 *    Aggregated bindings per key
 */


const KeyBinding CommonOwn_ESC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&CommonOwn_ESC_0000 }, 
};

const KeyBinding CommonOwn_BAK[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&CommonOwn_BAK_0000 }, 
};

const KeyBinding CommonOwn_MT[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&CommonOwn_MT_0000 }, 
};

const KeyBinding CommonOwn_FN[] PROGMEM =
{
  { MODE, {0x00, 0x00}, (void*)&CommonOwn_FN_0000 }, 
};

const KeyBinding CommonOwn_NUM[] PROGMEM =
{
  { MODE, {0x00, 0x00}, (void*)&CommonOwn_NUM_0000 }, 
};

const KeyBinding CommonOwn_RAL[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&CommonOwn_RAL_0000 }, 
};

const KeyBinding CommonOwn_SPC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&CommonOwn_SPC_0000 }, 
};

const KeyBinding CommonOwn_RCT[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&CommonOwn_RCT_0000 }, 
};

const KeyBinding CommonOwn_RSH[] PROGMEM =
{
  { MODIFIER, {0x00, 0x00}, (void*)&CommonOwn_RSH_0000 }, 
};

const KeyBinding CommonOwn_RET[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&CommonOwn_RET_0000 }, 
};

const KeyBinding CommonOwn_ALG[] PROGMEM =
{
  { MODE, {0x00, 0x00}, (void*)&CommonOwn_ALG_0000 }, 
};

const KeyBinding FnOwn_Q[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_Q_0000 }, 
};

const KeyBinding FnOwn_BRC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_BRC_0000 }, 
};

const KeyBinding FnOwn_R[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_R_0000 }, 
};

const KeyBinding FnOwn_U[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_U_0000 }, 
};

const KeyBinding FnOwn_T[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_T_0000 }, 
};

const KeyBinding FnOwn_BRO[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_BRO_0000 }, 
};

const KeyBinding FnOwn_Y[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_Y_0000 }, 
};

const KeyBinding FnOwn_ESC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_ESC_0000 }, 
};

const KeyBinding FnOwn_O[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_O_0000 }, 
};

const KeyBinding FnOwn_EQL[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&FnOwn_EQL_0000 }, 
};

const KeyBinding Own_A[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_A_0000 }, 
};

const KeyBinding Own_S[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_S_0000 }, 
};

const KeyBinding Own_BRC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_BRC_0000 }, 
};

const KeyBinding Own_B[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_B_0000 }, 
};

const KeyBinding Own_E[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_E_0000 }, 
};

const KeyBinding Own_D[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_D_0000 }, 
};

const KeyBinding Own_G[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_G_0000 }, 
};

const KeyBinding Own_BRO[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_BRO_0000 }, 
};

const KeyBinding Own_I[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_I_0000 }, 
};

const KeyBinding Own_H[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_H_0000 }, 
};

const KeyBinding Own_K[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_K_0000 }, 
};

const KeyBinding Own_J[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_J_0000 }, 
};

const KeyBinding Own_M[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_M_0000 }, 
};

const KeyBinding Own_L[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_L_0000 }, 
};

const KeyBinding Own_O[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_O_0000 }, 
};

const KeyBinding Own_N[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_N_0000 }, 
};

const KeyBinding Own_Q[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_Q_0000 }, 
};

const KeyBinding Own_P[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_P_0000 }, 
};

const KeyBinding Own_EQL[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_EQL_0000 }, 
};

const KeyBinding Own_R[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_R_0000 }, 
};

const KeyBinding Own_U[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_U_0000 }, 
};

const KeyBinding Own_T[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_T_0000 }, 
};

const KeyBinding Own_W[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_W_0000 }, 
};

const KeyBinding Own_V[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_V_0000 }, 
};

const KeyBinding Own_PER[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_PER_0000 }, 
};

const KeyBinding Own_X[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_X_0000 }, 
};

const KeyBinding Own_Y[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_Y_0000 }, 
};

const KeyBinding Own_Z[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_Z_0000 }, 
};

const KeyBinding Own_COM[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_COM_0000 }, 
};

const KeyBinding Own_QUE[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_QUE_0000 }, 
};

const KeyBinding Own_F[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_F_0000 }, 
};

const KeyBinding Own_C[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&Own_C_0000 }, 
};

const KeyBinding NumbersOwn_A[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_A_0000 }, 
};

const KeyBinding NumbersOwn_BRC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_BRC_0000 }, 
};

const KeyBinding NumbersOwn_E[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_E_0000 }, 
};

const KeyBinding NumbersOwn_D[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_D_0000 }, 
};

const KeyBinding NumbersOwn_G[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_G_0000 }, 
};

const KeyBinding NumbersOwn_F[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_F_0000 }, 
};

const KeyBinding NumbersOwn_I[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_I_0000 }, 
};

const KeyBinding NumbersOwn_H[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_H_0000 }, 
};

const KeyBinding NumbersOwn_K[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_K_0000 }, 
};

const KeyBinding NumbersOwn_J[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_J_0000 }, 
};

const KeyBinding NumbersOwn_L[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_L_0000 }, 
};

const KeyBinding NumbersOwn_O[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_O_0000 }, 
};

const KeyBinding NumbersOwn_Q[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_Q_0000 }, 
};

const KeyBinding NumbersOwn_P[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_P_0000 }, 
};

const KeyBinding NumbersOwn_S[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_S_0000 }, 
};

const KeyBinding NumbersOwn_R[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_R_0000 }, 
};

const KeyBinding NumbersOwn_U[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_U_0000 }, 
};

const KeyBinding NumbersOwn_T[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_T_0000 }, 
};

const KeyBinding NumbersOwn_W[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_W_0000 }, 
};

const KeyBinding NumbersOwn_Z[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_Z_0000 }, 
};

const KeyBinding NumbersOwn_ESC[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_ESC_0000 }, 
};

const KeyBinding NumbersOwn_BRO[] PROGMEM =
{
  { MAP, {0x00, 0x00}, (void*)&NumbersOwn_BRO_0000 }, 
};



