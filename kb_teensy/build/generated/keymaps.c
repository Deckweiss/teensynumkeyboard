/*
                    The HumbleHacker Keyboard Project
                 Copyright � 2008-2010, David Whetstone
               david DOT whetstone AT humblehacker DOT com

  This file is a part of The HumbleHacker Keyboard Project.

  The HumbleHacker Keyboard Project is free software: you can redistribute
  it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  The HumbleHacker Keyboard Project is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
  Public License for more details.

  You should have received a copy of the GNU General Public License along
  with The HumbleHacker Keyboard Project.  If not, see
  <http://www.gnu.org/licenses/>.

*/

#include "keymaps.h"
#include "hid_usages.h"

const KeyBindingArray keymap_CommonOwn[] =
{

  /* col: 0 */
  /* row:0 loc:BRO */ {0, NULL},
  /* row:1 loc:QUE */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:BRC */ {0, NULL},
  /* row:1 loc:KEY */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:EQL */ {0, NULL},
  /* row:1 loc:RSH */ {1, &CommonOwn_RSH[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:PER */ {0, NULL},
  /* row:1 loc:FN */ {1, &CommonOwn_FN[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},
};


const KeyBindingArray keymap_FnOwn[] =
{

  /* col: 0 */
  /* row:0 loc:BRO */ {1, &FnOwn_BRO[0]},
  /* row:1 loc:QUE */ {1, &Own_QUE[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:BRC */ {1, &FnOwn_BRC[0]},
  /* row:1 loc:KEY */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:EQL */ {1, &FnOwn_EQL[0]},
  /* row:1 loc:RSH */ {1, &CommonOwn_RSH[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:PER */ {1, &Own_PER[0]},
  /* row:1 loc:FN */ {1, &CommonOwn_FN[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},
};


const KeyBindingArray keymap_Own[] =
{

  /* col: 0 */
  /* row:0 loc:BRO */ {1, &Own_BRO[0]},
  /* row:1 loc:QUE */ {1, &Own_QUE[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:BRC */ {1, &Own_BRC[0]},
  /* row:1 loc:KEY */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:EQL */ {1, &Own_EQL[0]},
  /* row:1 loc:RSH */ {1, &CommonOwn_RSH[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:PER */ {1, &Own_PER[0]},
  /* row:1 loc:FN */ {1, &CommonOwn_FN[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},
};

const KeyMap default_keymap = &keymap_Own[0];

const KeyBindingArray keymap_NumbersOwn[] =
{

  /* col: 0 */
  /* row:0 loc:BRO */ {1, &NumbersOwn_BRO[0]},
  /* row:1 loc:QUE */ {1, &Own_QUE[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 1 */
  /* row:0 loc:BRC */ {1, &NumbersOwn_BRC[0]},
  /* row:1 loc:KEY */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 2 */
  /* row:0 loc:EQL */ {1, &Own_EQL[0]},
  /* row:1 loc:RSH */ {1, &CommonOwn_RSH[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 3 */
  /* row:0 loc:PER */ {1, &Own_PER[0]},
  /* row:1 loc:FN */ {1, &CommonOwn_FN[0]},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 4 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 5 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 6 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},

  /* col: 7 */
  /* row:0 loc:-- */ {0, NULL},
  /* row:1 loc:-- */ {0, NULL},
  /* row:2 loc:-- */ {0, NULL},
  /* row:3 loc:-- */ {0, NULL},
  /* row:4 loc:-- */ {0, NULL},
  /* row:5 loc:-- */ {0, NULL},
  /* row:6 loc:-- */ {0, NULL},
  /* row:7 loc:-- */ {0, NULL},
  /* row:8 loc:-- */ {0, NULL},
  /* row:9 loc:-- */ {0, NULL},
  /* row:10 loc:-- */ {0, NULL},
  /* row:11 loc:-- */ {0, NULL},
};


