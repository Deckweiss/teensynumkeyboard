/*
                    The HumbleHacker Keyboard Project
                 Copyright � 2008-2010, David Whetstone
               david DOT whetstone AT humblehacker DOT com

  This file is a part of The HumbleHacker Keyboard Project.

  The HumbleHacker Keyboard Project is free software: you can redistribute
  it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  The HumbleHacker Keyboard Project is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
  Public License for more details.

  You should have received a copy of the GNU General Public License along
  with The HumbleHacker Keyboard Project.  If not, see
  <http://www.gnu.org/licenses/>.

*/

#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <stdint.h>
#include <avr/io.h>

#define NUM_ROWS 12
#define NUM_COLS 8

struct Cell
{
  uint8_t row;
  uint8_t col;
};

typedef struct Cell Cell;

static inline
uint8_t
cell_to_index(Cell cell)
{
  return cell.col * NUM_ROWS + cell.row;
}


static inline
void
activate_row(uint8_t row)
{
  // set all row pins as inputs
  DDRE &= ~(1 << PE0);
  DDRE &= ~(1 << PE1);
  DDRC &= ~(1 << PC0);
  DDRC &= ~(1 << PC1);
  DDRC &= ~(1 << PC2);
  DDRC &= ~(1 << PC3);
  DDRF &= ~(1 << PF2);
  DDRF &= ~(1 << PF3);
  DDRF &= ~(1 << PF4);
  DDRF &= ~(1 << PF5);
  DDRF &= ~(1 << PF6);
  DDRF &= ~(1 << PF7);

  // set current row pin as output
  switch (row)
  {
    case 0: DDRE |= (1 << PE0); break;
    case 1: DDRE |= (1 << PE1); break;
    case 2: DDRC |= (1 << PC0); break;
    case 3: DDRC |= (1 << PC1); break;
    case 4: DDRC |= (1 << PC2); break;
    case 5: DDRC |= (1 << PC3); break;
    case 6: DDRF |= (1 << PF2); break;
    case 7: DDRF |= (1 << PF3); break;
    case 8: DDRF |= (1 << PF4); break;
    case 9: DDRF |= (1 << PF5); break;
    case 10: DDRF |= (1 << PF6); break;
    case 11: DDRF |= (1 << PF7); break;
  }

  // drive all row pins high
  PORTE |= (1 << PE0);
  PORTE |= (1 << PE1);
  PORTC |= (1 << PC0);
  PORTC |= (1 << PC1);
  PORTC |= (1 << PC2);
  PORTC |= (1 << PC3);
  PORTF |= (1 << PF2);
  PORTF |= (1 << PF3);
  PORTF |= (1 << PF4);
  PORTF |= (1 << PF5);
  PORTF |= (1 << PF6);
  PORTF |= (1 << PF7);

  // drive current row pin low
  switch (row)
  {
    case 0: PORTE &= ~(1 << PE0); break;
    case 1: PORTE &= ~(1 << PE1); break;
    case 2: PORTC &= ~(1 << PC0); break;
    case 3: PORTC &= ~(1 << PC1); break;
    case 4: PORTC &= ~(1 << PC2); break;
    case 5: PORTC &= ~(1 << PC3); break;
    case 6: PORTF &= ~(1 << PF2); break;
    case 7: PORTF &= ~(1 << PF3); break;
    case 8: PORTF &= ~(1 << PF4); break;
    case 9: PORTF &= ~(1 << PF5); break;
    case 10: PORTF &= ~(1 << PF6); break;
    case 11: PORTF &= ~(1 << PF7); break;
  }
}

static inline
uint32_t
read_row_data(void)
{
  uint32_t cols = 0;

  if ((~PINF)&(1<<PF1)) cols |= (1UL<< 0);
  if ((~PINB)&(1<<PB4)) cols |= (1UL<< 1);
  if ((~PINB)&(1<<PB5)) cols |= (1UL<< 2);
  if ((~PINB)&(1<<PB6)) cols |= (1UL<< 3);
  if ((~PIND)&(1<<PD6)) cols |= (1UL<< 4);
  if ((~PIND)&(1<<PD5)) cols |= (1UL<< 5);
  if ((~PIND)&(1<<PD7)) cols |= (1UL<< 6);
  if ((~PIND)&(1<<PD4)) cols |= (1UL<< 7);

  return cols;
}


static inline
void
init_cols(void)
{
  /* Columns are inputs */
  DDRF &= ~(1 << PF1);
  DDRB &= ~(1 << PB4);
  DDRB &= ~(1 << PB5);
  DDRB &= ~(1 << PB6);
  DDRD &= ~(1 << PD6);
  DDRD &= ~(1 << PD5);
  DDRD &= ~(1 << PD7);
  DDRD &= ~(1 << PD4);

  /* Enable pull-up resistors on inputs */
  PORTF |= (1 << PF1);
  PORTB |= (1 << PB4);
  PORTB |= (1 << PB5);
  PORTB |= (1 << PB6);
  PORTD |= (1 << PD6);
  PORTD |= (1 << PD5);
  PORTD |= (1 << PD7);
  PORTD |= (1 << PD4);
}

#endif /* __MATRIX_H__ */

