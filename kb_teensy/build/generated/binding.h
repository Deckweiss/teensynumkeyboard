/*
                    The HumbleHacker Keyboard Project
                 Copyright � 2008-2010, David Whetstone
               david DOT whetstone AT humblehacker DOT com

  This file is a part of The HumbleHacker Keyboard Project.

  The HumbleHacker Keyboard Project is free software: you can redistribute
  it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  The HumbleHacker Keyboard Project is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
  Public License for more details.

  You should have received a copy of the GNU General Public License along
  with The HumbleHacker Keyboard Project.  If not, see
  <http://www.gnu.org/licenses/>.

*/

#ifndef __BINDING_H__
#define __BINDING_H__

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdbool.h>
#include <stdint.h>
#include "hid_usages.h"
#include "matrix.h"

typedef struct ModeTarget ModeTarget;
typedef struct MacroTarget MacroTarget;
typedef struct MapTarget MapTarget;
typedef struct ModifierTarget ModifierTarget;
typedef struct KeyBinding KeyBinding;
typedef struct KeyBindingArray KeyBindingArray;
typedef struct PreMods PreMods;
typedef const KeyBindingArray* KeyMap;

/*
 *    PreMods
 */

struct PreMods
{
  uint8_t std;
  uint8_t any;
};

uint8_t PreMods__compare(const PreMods *this, uint8_t mods);
bool PreMods__is_empty(const PreMods *this);

/*
 *    KeyBinding
 */

struct KeyBinding
{
  enum {NOMAP, MAP, MODE, MACRO, MODIFIER} kind;
  PreMods premods;
  void *target;
};

void                  KeyBinding__copy(const KeyBinding *this, KeyBinding *dst);
const ModeTarget*     KeyBinding__get_mode_target(const KeyBinding *this);
const MacroTarget*    KeyBinding__get_macro_target(const KeyBinding *this);
const MapTarget*      KeyBinding__get_map_target(const KeyBinding *this);
const ModifierTarget* KeyBinding__get_modifier_target(const KeyBinding *this);

/*
 *    KeyBindingArray
 */

struct KeyBindingArray
{
  uint8_t length;
  const KeyBinding *data;
} ;

const KeyBinding* KeyBindingArray__get_binding(const KeyBindingArray *this, uint8_t index);

static inline
void
KeyBindingArray__get(KeyBindingArray *array, const KeyBindingArray *from)
{
  memcpy_P((void*)array, (PGM_VOID_P)from, sizeof(KeyBindingArray));
}

/*
 *    ModeTarget
 */

struct ModeTarget
{
  enum {MOMENTARY, TOGGLE} type;
  KeyMap mode_map;
};

/*
 *    MapTarget
 */

struct MapTarget
{
  uint8_t modifiers;
  Usage usage;
};

/*
 *    ModifierTarget
 */

struct ModifierTarget
{
  Modifier modifier;
};

/*
 *    MacroTarget
 */

struct MacroTarget
{
  uint8_t length;
  const MapTarget *targets;
};

const MapTarget* MacroTarget__get_map_target(const MacroTarget *this, uint8_t index);

/*
 *    Binding declarations
 */

extern const KeyBinding CommonOwn_ESC[] PROGMEM;
extern const KeyBinding CommonOwn_BAK[] PROGMEM;
extern const KeyBinding CommonOwn_MT[] PROGMEM;
extern const KeyBinding CommonOwn_FN[] PROGMEM;
extern const KeyBinding CommonOwn_NUM[] PROGMEM;
extern const KeyBinding CommonOwn_RAL[] PROGMEM;
extern const KeyBinding CommonOwn_SPC[] PROGMEM;
extern const KeyBinding CommonOwn_RCT[] PROGMEM;
extern const KeyBinding CommonOwn_RSH[] PROGMEM;
extern const KeyBinding CommonOwn_RET[] PROGMEM;
extern const KeyBinding CommonOwn_ALG[] PROGMEM;
extern const KeyBinding FnOwn_Q[] PROGMEM;
extern const KeyBinding FnOwn_BRC[] PROGMEM;
extern const KeyBinding FnOwn_R[] PROGMEM;
extern const KeyBinding FnOwn_U[] PROGMEM;
extern const KeyBinding FnOwn_T[] PROGMEM;
extern const KeyBinding FnOwn_BRO[] PROGMEM;
extern const KeyBinding FnOwn_Y[] PROGMEM;
extern const KeyBinding FnOwn_ESC[] PROGMEM;
extern const KeyBinding FnOwn_O[] PROGMEM;
extern const KeyBinding FnOwn_EQL[] PROGMEM;
extern const KeyBinding Own_A[] PROGMEM;
extern const KeyBinding Own_S[] PROGMEM;
extern const KeyBinding Own_BRC[] PROGMEM;
extern const KeyBinding Own_B[] PROGMEM;
extern const KeyBinding Own_E[] PROGMEM;
extern const KeyBinding Own_D[] PROGMEM;
extern const KeyBinding Own_G[] PROGMEM;
extern const KeyBinding Own_BRO[] PROGMEM;
extern const KeyBinding Own_I[] PROGMEM;
extern const KeyBinding Own_H[] PROGMEM;
extern const KeyBinding Own_K[] PROGMEM;
extern const KeyBinding Own_J[] PROGMEM;
extern const KeyBinding Own_M[] PROGMEM;
extern const KeyBinding Own_L[] PROGMEM;
extern const KeyBinding Own_O[] PROGMEM;
extern const KeyBinding Own_N[] PROGMEM;
extern const KeyBinding Own_Q[] PROGMEM;
extern const KeyBinding Own_P[] PROGMEM;
extern const KeyBinding Own_EQL[] PROGMEM;
extern const KeyBinding Own_R[] PROGMEM;
extern const KeyBinding Own_U[] PROGMEM;
extern const KeyBinding Own_T[] PROGMEM;
extern const KeyBinding Own_W[] PROGMEM;
extern const KeyBinding Own_V[] PROGMEM;
extern const KeyBinding Own_PER[] PROGMEM;
extern const KeyBinding Own_X[] PROGMEM;
extern const KeyBinding Own_Y[] PROGMEM;
extern const KeyBinding Own_Z[] PROGMEM;
extern const KeyBinding Own_COM[] PROGMEM;
extern const KeyBinding Own_QUE[] PROGMEM;
extern const KeyBinding Own_F[] PROGMEM;
extern const KeyBinding Own_C[] PROGMEM;
extern const KeyBinding NumbersOwn_A[] PROGMEM;
extern const KeyBinding NumbersOwn_BRC[] PROGMEM;
extern const KeyBinding NumbersOwn_E[] PROGMEM;
extern const KeyBinding NumbersOwn_D[] PROGMEM;
extern const KeyBinding NumbersOwn_G[] PROGMEM;
extern const KeyBinding NumbersOwn_F[] PROGMEM;
extern const KeyBinding NumbersOwn_I[] PROGMEM;
extern const KeyBinding NumbersOwn_H[] PROGMEM;
extern const KeyBinding NumbersOwn_K[] PROGMEM;
extern const KeyBinding NumbersOwn_J[] PROGMEM;
extern const KeyBinding NumbersOwn_L[] PROGMEM;
extern const KeyBinding NumbersOwn_O[] PROGMEM;
extern const KeyBinding NumbersOwn_Q[] PROGMEM;
extern const KeyBinding NumbersOwn_P[] PROGMEM;
extern const KeyBinding NumbersOwn_S[] PROGMEM;
extern const KeyBinding NumbersOwn_R[] PROGMEM;
extern const KeyBinding NumbersOwn_U[] PROGMEM;
extern const KeyBinding NumbersOwn_T[] PROGMEM;
extern const KeyBinding NumbersOwn_W[] PROGMEM;
extern const KeyBinding NumbersOwn_Z[] PROGMEM;
extern const KeyBinding NumbersOwn_ESC[] PROGMEM;
extern const KeyBinding NumbersOwn_BRO[] PROGMEM;

#endif // __BINDING_H__
